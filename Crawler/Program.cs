﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;

class Program
{
    static Queue<Uri> queue = new Queue<Uri>();
    static Dictionary<int, int[]> graph = new Dictionary<int, int[]>();
    static Dictionary<Uri, int> span = new Dictionary<Uri, int>();
    
    static Uri[] GetLinks(Uri url)
    {
        try
        {
            var client = new WebClient();
            var page = client.DownloadString(url);
            var doc = new HtmlDocument();
            doc.LoadHtml(page);
            return doc.DocumentNode.SelectNodes("//a[@href]")
                .Select(node => node.GetAttributeValue("href", ""))
                .Select(href => new Uri(href, UriKind.RelativeOrAbsolute))
                .Select(uri => !uri.IsAbsoluteUri ? new Uri(url, uri) : uri)
                .ToArray();
        }
        catch (Exception)
        {
            return new Uri[] { };
        }
    }

    static void SerializeGraph()
    {
        var lines = graph
            .ToList()
            .SelectMany(p => p.Value.Select(u => new { first = p.Key, second = u }))
            .Select(v => v.first + " " + v.second)
            .ToList();
        var res = graph.Keys.Count + " " + graph.Keys.Count + " " + lines.Count + "\n" + string.Join("\n", lines);
        File.WriteAllText("mat.mm", res);
        File.WriteAllText("map.txt", string.Join("\n", span.ToList().Select(p => p.Key + " " + p.Value)));
    }

    static string[] domains = new[] { "en.wikipedia.org" };
    static string[] bannedExts = new[] { "pdf", "gif", "jpg", "png" };
    static void ProcessUri(Uri uri)
    {
        Console.WriteLine(uri);
        var links = GetLinks(uri);
        lock (graph)
        {
            var cleanedLinks = links
                    .Select(u => new Uri(u.GetLeftPart(UriPartial.Path)))
                    .Where(u => !bannedExts.Any(e => u.AbsolutePath.ToLowerInvariant().EndsWith("." + e)))
                    .Distinct()
                    .Where(link => domains.Contains(link.Authority));
            if (span.Count < 50000)
            {
                var newLinks = cleanedLinks
                    .Where(link => !span.ContainsKey(link))
                    .ToList();
                newLinks.ForEach(link => { span.Add(link, span.Count); queue.Enqueue(link); });
            }
            graph[span[uri]] = cleanedLinks
                .Where(link => span.ContainsKey(link))
                .Select(link => span[link])
                .ToArray();
        }
    }
    static void Main()
    {
        var root = new Uri("http://en.wikipedia.org");
        queue.Enqueue(root);
        span.Add(root, 0);
        var count = 0;
        while (true)
        {
            var uris = new List<Uri>();
            while (uris.Count < 20 && queue.Count > 0) uris.Add(queue.Dequeue());
            Parallel.ForEach(uris, ProcessUri);
            Console.WriteLine(graph.Count);
            count++;
            if (count % 10 == 0)
            {
                SerializeGraph();
            }
            if (uris.Count == 0) break;
        }
        SerializeGraph();

        var lines = File.ReadAllLines("math/mat.mm");
        int width = int.Parse(lines[0].Split(' ')[0]), height = int.Parse(lines[0].Split(' ')[1]);
        var bmp = new Bitmap(1000, 1000);
        var gfx = Graphics.FromImage(bmp);
        gfx.Clear(Color.White);
        for (int i = 1; i < lines.Length; ++i)
        {
            int x = int.Parse(lines[i].Split(' ')[0]), y = int.Parse(lines[i].Split(' ')[1]);
            gfx.FillRectangle(new SolidBrush(Color.Black), (float)x / width * 1000, (float)y / height * 1000, 1, 1);
        }
        bmp.Save("plot.png");
        //Top10();
    }

    static void Top10()
    {
        var map = File.ReadAllLines("wiki50k/map.txt")
            .Select((line, i) => Tuple.Create(i, line.Split(' ')[0]))
            .ToDictionary(t => t.Item1, t => t.Item2);
        var lines = File.ReadAllLines("wiki50k/wiki.txt")
            .Select((line, i) => Tuple.Create(double.Parse(line, NumberFormatInfo.InvariantInfo), i))
            .OrderBy(t => t.Item1)
            .ToList()
            .Select(t => (-t.Item1) + " " + map[t.Item2]);
        File.WriteAllText("top.txt", string.Join("\n", lines));
    }
}